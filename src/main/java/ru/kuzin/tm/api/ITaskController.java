package ru.kuzin.tm.api;

public interface ITaskController {
    void showTasks();

    void createTasks();

    void clearTasks();
}
