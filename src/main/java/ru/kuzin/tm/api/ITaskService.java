package ru.kuzin.tm.api;

import ru.kuzin.tm.model.Task;

public interface ITaskService extends ITaskRepository{

    Task create(String name, String description);

}
