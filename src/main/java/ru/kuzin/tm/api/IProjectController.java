package ru.kuzin.tm.api;

public interface IProjectController {
    void showProjects();

    void createProjects();

    void clearProjects();
}
