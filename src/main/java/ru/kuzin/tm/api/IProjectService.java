package ru.kuzin.tm.api;

import ru.kuzin.tm.model.Project;

import java.util.List;

public interface IProjectService extends IProjectRepository{

    Project create(String name, String description);

}
